﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCScript : MonoBehaviour
{
    private ObjectManager OM;
    private CameraMover CM;

    private int whosGuessing; //1 is mum, 2 is dad
    private int whatPhaseOfTheSearch; //0 is the first "I spy", 1 is the searching time of 5 seconds, 2 is the second colour hint, and 3 is the second searching time
    private int whatObject; //from 0 to 4
    private int once, onceV2;
    private List<Item> whatWeWillBeAsking = new List<Item>();
    public float timer;
    public float timeAllowed = 20;

    public bool gameOver;
    public string[] _whatISee;

    public Text mumsISpyWriting, dadsISpyWriting;
    public Text mumsLetter, dadsLetter;
    public Slider slider;

    // Start is called before the first frame update
    void Start()
    {
        OM = GameObject.Find("Object Manager").GetComponent<ObjectManager>();
        CM = GameObject.Find("Camera").GetComponent<CameraMover>();
        while (whatWeWillBeAsking.Count < 5)
        {
            Item it = OM.GetRandomItem();
            if (!whatWeWillBeAsking.Contains(it))
                whatWeWillBeAsking.Add(it);
            
        }
        whosGuessing = 1;
        whatPhaseOfTheSearch = 0;
        whatObject = 0;
        gameOver = false;
        onceV2 = 1;
        slider.value = SliderDisplay();
    }

    // Update is called once per frame
    void Update()
    {
        if (whatObject >= 5)
            gameOver = true;

        if (!gameOver)
        {
            slider.value = SliderDisplay();
            if (whosGuessing == 1)
            {
                if (whatPhaseOfTheSearch == 0)
                {
                    //print("Mummy's guessing now"); //mother's turn
                    mumsISpyWriting.GetComponent<TypewritingEffect>().BeginText("I spy with my little eye something beginning with a " + whatWeWillBeAsking[whatObject].GetFirstLetter() + " ", whatWeWillBeAsking[whatObject].GetFirstLetter().ToString());
                    GameObject.Find("SoundManager").GetComponent<SoundManager>().Play("MumIspy");
                    //print("I spy with my little eye something beginning with a " + whatWeWillBeAsking[whatObject].GetFirstLetter()); // now it needs to go to the next jump
                    //mumsISpyWriting.GetComponent<TypewritingEffect>().EndText(10f);
                    //mumsLetter.text = whatWeWillBeAsking[whatObject].GetFirstLetter().ToString();
                    CM.SetCurrentGuess(whatWeWillBeAsking[whatObject].GetFirstLetter());
                    whatPhaseOfTheSearch++;
                    once = 1;
                }
                else if (whatPhaseOfTheSearch == 1)
                {
                    if (once == 1)
                    {
                        timer = timeAllowed;
                        once--;
                    }
                    //print("entering phase 1");
                    //print("I'm giving you ten seconds to find this");
                    if (timer > 0)
                    {
                        timer -= Time.deltaTime;
                        //print(timer);
                    }
                    if (timer <= 0)
                    {
                        whatPhaseOfTheSearch++;
                    }
                } else if (whatPhaseOfTheSearch == 2)
                {
                    //print("times up");
                    //print("Okay, maybe it'll help to know that the item is " + whatWeWillBeAsking[whatObject].GetColour());
                    mumsISpyWriting.GetComponent<TypewritingEffect>().BeginTextWithColour("Okay, maybe it'll help to know that the item is " + whatWeWillBeAsking[whatObject].GetColour() + " ", whatWeWillBeAsking[whatObject].GetColour(), whatWeWillBeAsking[whatObject].GetFirstLetter().ToString());
                    //print(whatWeWillBeAsking[whatObject].GetColour());
                    whatPhaseOfTheSearch++;
                    once = 1;
                }
                else if (whatPhaseOfTheSearch == 3)
                {
                    if (once == 1)
                    {
                        timer = timeAllowed;
                        once--;
                    }
                    //print("entering phase 2");
                    //print("I'm giving you ten seconds to find this");
                    if (timer > 0)
                    {
                        timer -= Time.deltaTime;
                        //print(timer);
                    }
                    if (timer <= 0)
                    {
                        whatPhaseOfTheSearch = 0;
                        //print("times up");
                        //print("Sorry, you didn't get it. The thing I saw was " + whatWeWillBeAsking[whatObject]);
                        mumsISpyWriting.GetComponent<TypewritingEffect>().BeginText("Sorry, you didn't get it. The thing I saw was " + whatWeWillBeAsking[whatObject].GetName() + " ", "");

                        mumsISpyWriting.text = "";
                        mumsLetter.text = "";
                        mumsLetter.color = new Color(0, 0, 0);
                        whatObject++;
                        whosGuessing = 2;
                    }
                }
            }
            else
            {
                if (whatPhaseOfTheSearch == 0)
                {
                    //print("Daddy's guessing now"); //mother's turn
                    //print("I spy with my little eye something beginning with a " + whatWeWillBeAsking[whatObject].GetFirstLetter()); // now it needs to go to the next jump
                    dadsISpyWriting.GetComponent<TypewritingEffect>().BeginText("I spy with my little eye something beginning with a " + whatWeWillBeAsking[whatObject].GetFirstLetter() + " ", whatWeWillBeAsking[whatObject].GetFirstLetter().ToString());
                    GameObject.Find("SoundManager").GetComponent<SoundManager>().Play("DadIspy");
                    CM.SetCurrentGuess(whatWeWillBeAsking[whatObject].GetFirstLetter());
                    whatPhaseOfTheSearch++;
                    once = 1;
                }
                else if (whatPhaseOfTheSearch == 1)
                {
                    if (once == 1)
                    {
                        timer = timeAllowed;
                        once--;
                    }
                    //print("entering phase 1");
                    //print("I'm giving you ten seconds to find this");
                    if (timer > 0)
                    {
                        timer -= Time.deltaTime;
                        //print(timer);
                    }
                    if (timer <= 0)
                    {
                        whatPhaseOfTheSearch++; //2
                        
                    }
                }
                else if (whatPhaseOfTheSearch == 2)
                {
                    //print("times up");
                    //print("Okay, maybe it'll help to know that the item is " + whatWeWillBeAsking[whatObject].GetColour());
                    dadsISpyWriting.GetComponent<TypewritingEffect>().BeginTextWithColour("Okay, maybe it'll help to know that the item is " + whatWeWillBeAsking[whatObject].GetColour() + " ", whatWeWillBeAsking[whatObject].GetColour(), whatWeWillBeAsking[whatObject].GetFirstLetter().ToString());
                    whatPhaseOfTheSearch++;
                    once = 1;
                }
                else if (whatPhaseOfTheSearch == 3)
                {
                    if (once == 1)
                    {
                        timer = timeAllowed;
                        once--;
                    }
                    //print("entering phase 2");
                    //print("I'm giving you ten seconds to find this");
                    if (timer > 0)
                    {
                        timer -= Time.deltaTime;
                        //print(timer);
                    }
                    if (timer <= 0)
                    {
                        whatPhaseOfTheSearch = 0;
                        //print("times up");
                        //print("Sorry, you didn't get it. The thing I saw was " + whatWeWillBeAsking[whatObject]);
                        dadsISpyWriting.GetComponent<TypewritingEffect>().BeginText("Sorry, you didn't get it. The thing I saw was " + whatWeWillBeAsking[whatObject].GetName() + " ", "");
                        whatObject++;
                        dadsISpyWriting.text = "";
                        dadsLetter.text = "";
                        dadsLetter.color = new Color(0, 0, 0);
                        whosGuessing = 1;
                    }
                }
            }
        } else
        {
            if (onceV2 == 1)
            {
                mumsISpyWriting.GetComponent<TypewritingEffect>().BeginText("Ok, I think that's enough for one trip. ", "");
                onceV2--;
            }
            
        }
    
    }

    public void GuessingHere (Item obj)
    {
        print("I'm guessing here, mum!");
        if (obj.GetFirstLetter() == whatWeWillBeAsking[whatObject].GetFirstLetter() /*&& obj.GetColour() == whatWeWillBeAsking[whatObject].GetColour()*/)
        {
            print("guessed right");
            GameObject.Find("SoundManager").GetComponent<SoundManager>().Play("Right");
            whatPhaseOfTheSearch = 0;
            whatObject++;
            if (whosGuessing == 1) {
                mumsISpyWriting.GetComponent<TypewritingEffect>().BeginText("Yay, you got it ", "");
                whosGuessing = 2;
            }
            else
            {
                dadsISpyWriting.GetComponent<TypewritingEffect>().BeginText("You got it right ", "");
                whosGuessing = 1;
            }
        } else
        {
            print("guessed wrong");
            if (whosGuessing == 1)
            {
                mumsISpyWriting.GetComponent<TypewritingEffect>().BeginText("Sorry, that isn't it ", "");
                print("mum sees that you're wrong");
            } else
            {
                dadsISpyWriting.GetComponent<TypewritingEffect>().BeginText("Nope kiddo, not this one ", "");

            }
            //print("You guessed wrong");
            if (whatPhaseOfTheSearch < 2)
            {
                whatPhaseOfTheSearch = 2;
            } else
            {
                //print("oh well, no points for you");
                if (whosGuessing == 1)
                {
                    mumsISpyWriting.GetComponent<TypewritingEffect>().BeginText("How about your father guesses? ", "");
                    //The next line doesn't work
                    dadsISpyWriting.GetComponent<TypewritingEffect>().BeginText("I bet it's the " + whatWeWillBeAsking[whatObject].GetName() + " ", "");
                }
                else
                {
                    dadsISpyWriting.GetComponent<TypewritingEffect>().BeginText("Dear, do you want to give this a try? ", "");
                    //the next line doesn't work
                    mumsISpyWriting.GetComponent<TypewritingEffect>().BeginText("I think it's the " + whatWeWillBeAsking[whatObject].GetName() + " ", "");
                }
                whatPhaseOfTheSearch = 0;
                if(whatObject < 4)
                    whatObject++;
                if (whosGuessing == 1)
                    whosGuessing = 2;
                else
                {
                    whosGuessing = 1;
                }
            }
        }
    }

    float SliderDisplay ()
    {
        return timer / timeAllowed;
        //print("you working, bro?");
    }
}
