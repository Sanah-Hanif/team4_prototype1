﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public string itemName;
    public string[] colours;

    public string GetName()
    {
        return itemName;
    }

    public char GetFirstLetter()
    {
        return itemName[0];
    }

    public GameObject GetObject()
    {
        return this.gameObject;
    }

    public bool CheckIfItIsThisColour(string col)
    {
        foreach (string c in colours)
        {
            if (col == c)
                return true;
        }
        return false;
    }

    public string GetColour()
    {
        return colours[0];
    }
}
