﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypewritingEffect : MonoBehaviour
{
    //credit to Creagines https://www.youtube.com/watch?v=1qbjmb_1hV4
    public float delay = 0.05f;
    private string _fullText, _currentLetter;
    private string currentText = "";
    Text myText;
    public Text myLetterHolder;

    // Start is called before the first frame update
    void Start()
    {
        myText = this.GetComponent<Text>();
    }

    public void BeginText(string message, string whatLetter)
    {
        myLetterHolder.text = "";
        myLetterHolder.color = new Color(0, 0, 0);
        _fullText = message;
        _currentLetter = whatLetter;
        StartCoroutine(ShowText());
    }

    public void EndText(float time)
    {
        _fullText = "";
        Invoke("CutText", time);
    }

    IEnumerator ShowText()
    {
        for (int i = 0; i < _fullText.Length + 1; i++)
        {
            currentText = _fullText.Substring(0, i);
            myText.text = currentText;
            if (i == _fullText.Length)
            {
                myText.text = "";
                myLetterHolder.text = _currentLetter;
            }
            yield return new WaitForSeconds(delay);
        }
    }

    public void BeginTextWithColour(string message, string whatColour, string whatLetter)
    {
        myLetterHolder.text = "";
        _fullText = message;
        _currentLetter = whatLetter;
        //print("Notice me Senpai!");
        switch (whatColour)
        {
            case "Black":
                myLetterHolder.color = new Color(0, 0, 0);
                break;
            case "White":
                myLetterHolder.color = new Color(1, 1, 1);
                break;
            case "Brown":
                myLetterHolder.color = new Color(0.82f, 0.41f, 0.12f);
                break;
            case "Grey":
                myLetterHolder.color = new Color(0.5f, 0.5f, 0.5f);
                break;
            case "Yellow":
                myLetterHolder.color = new Color(1f, 1f, 0f);
                break;
            case "Blue":
                myLetterHolder.color = new Color(0f, 0f, 1f);
                break;
            case "Red":
                myLetterHolder.color = new Color(1f, 0f, 0f);
                break;
            case "Orange":
                myLetterHolder.color = new Color(1f, 0.5f, 0f);
                break;
            default:
                print("you forgot a colour, Sanah");
                break;
        }
        StartCoroutine(ShowText());
    }
}

//switch (whatWeWillBeAsking[whatObject].GetColour()) {
//                        case "Black":
//                            mumsLetter.color = new Color(0, 0, 0);
//                            break;
//                        case "White":
//                            mumsLetter.color = new Color(1, 1, 1);
//                            break;
//                        case "Brown":
//                            mumsLetter.color = new Color(0.82f, 0.41f, 0.12f);
//                            break;
//                        case "Grey":
//                            mumsLetter.color = new Color(0.5f, 0.5f, 0.5f);
//                            break;
//                        case "Yellow":
//                            mumsLetter.color = new Color(1f, 1f, 0f);
//                            break;
//                        case "Blue":
//                            mumsLetter.color = new Color(0f, 0f, 1f);
//                            break;
//                        case "Red":
//                            mumsLetter.color = new Color(1f, 0f, 0f);
//                            break;
//                        case "Orange":
//                            mumsLetter.color = new Color(1f, 0.5f, 0f);
//                            break;
//                        default:
//                            print("you forgot a colour, Sanah");
//                            break;
