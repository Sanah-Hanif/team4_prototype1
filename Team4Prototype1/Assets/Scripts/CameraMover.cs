﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public float horizontalSpeed, verticalSpeed;
    private float _horizontalMovement, _verticalMovement;
    private GameObject _backBone;
    float _angleOfInclination;

    public char currentGuess; //the current guess for the first letter
    public string currentColour; //if there isn't a letter, then it will check the colour
    private NPCScript NPCS;

    public float upperX, lowerX, upperY, lowerY;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _backBone = GameObject.Find("Backbone");
        _angleOfInclination = _backBone.transform.eulerAngles.z;
        NPCS = GameObject.Find("NPC Controller").GetComponent<NPCScript>();
        //transform.RotateAround(_backBone.transform.position, Vector3.right, 20 * Time.deltaTime);
    }

    // Update is called once per frame
    void Update()
    {
        _horizontalMovement += horizontalSpeed * Input.GetAxis("Mouse X"); //the movement is added to the players rotation in the y-axis
        _verticalMovement -= verticalSpeed * Input.GetAxis("Mouse Y"); //the movement is suctracted in the x-direction

        _horizontalMovement = Mathf.Clamp(_horizontalMovement, lowerX, upperX);
        //clamping rotation in the horizontal direction in degrees
        _verticalMovement = Mathf.Clamp(_verticalMovement, lowerY, upperY);
        //clamping rotation in the vertical direction in degrees

        transform.eulerAngles = new Vector3(Mathf.Cos(Mathf.Deg2Rad * _angleOfInclination) * _verticalMovement + Mathf.Sin(Mathf.Deg2Rad * _angleOfInclination) * _horizontalMovement, Mathf.Cos(Mathf.Deg2Rad * _angleOfInclination) * _horizontalMovement + Mathf.Sin(Mathf.Deg2Rad * _angleOfInclination) * _verticalMovement, _angleOfInclination);

        if (Input.GetButtonDown("Fire1"))
        {
            WhatchaPointingAt();
        }
    }

    void WhatchaPointingAt()
    {
        RaycastHit hit;
        if (Physics.Raycast(this.transform.position, this.transform.forward, out hit))
        {
            //print(hit.transform);
            Item it = GameObject.Find("Object Manager").GetComponent<ObjectManager>().FindItemWithGivenObject(hit.transform.gameObject);
            if (it != null)
            {
                NPCS.GuessingHere(hit.transform.gameObject.GetComponent<Item>());
            }
        }
    }

    public void SetCurrentGuess (char obj)
    {
        currentGuess = obj;
    }
}
